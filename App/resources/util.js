export function angleFromCoordinate(lat1, lng1, lat2, lng2) {
  let fLat = degreeToRadians(lat1);
  let fLong = degreeToRadians(lng1);
  let tLat = degreeToRadians(lat2);
  let tLong = degreeToRadians(lng2);

  let dLon = tLong - fLong;

  let degree = radiansToDegree(
    Math.atan2(
      Math.sin(dLon) * Math.cos(tLat),
      Math.cos(fLat) * Math.sin(tLat) -
        Math.sin(fLat) * Math.cos(tLat) * Math.cos(dLon),
    ),
  );

  if (degree >= 0) {
    return degree;
  } else {
    return 360 + degree;
  }
}

export function degreeToRadians(latLong) {
  return (Math.PI * latLong) / 180.0;
}

export function radiansToDegree(latLong) {
  return (latLong * 180.0) / Math.PI;
}
export function getAtan2(y, x) {
  return Math.atan2(y, x);
}

export function vehicleBearing(endpoint, startpoint) {
  let x1 = endpoint.latitude;
  let y1 = endpoint.longitude;
  let x2 = startpoint.latitude;
  let y2 = startpoint.longitude;

  var radians = getAtan2(y1 - y2, x1 - x2);

  var compassReading = radians * (180 / Math.PI);

  var coordNames = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW', 'N'];
  var coordIndex = Math.round(compassReading / 45);
  if (coordIndex < 0) {
    coordIndex = coordIndex + 8;
  }

  return coordNames[coordIndex]; // returns the coordinate value
}
