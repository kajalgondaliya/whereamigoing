const icons = {
  ARROW: require('../resources/images/arrow.png'),
  COMPASS: require('../resources/images/compass.png'),
  LOCATION_LIST: require('../resources/images/location_list.png'),
};
export default icons;
