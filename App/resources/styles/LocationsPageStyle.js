import {StyleSheet} from 'react-native';
import colors from '../colors';
const LocationsPageStyle = StyleSheet.create({
  container: {
    backgroundColor: colors.colorBackground,
    flex: 1,
  },
  row: {flexDirection: 'row'},
  viewFlex: {flex: 1},
  textHeader: {
    color: colors.black,
    fontSize: 20,
    padding: 10,
    textAlign: 'center',
    alignSelf: 'center',
  },
  viewSeprator: {
    height: 1,
    marginStart: 20,
    marginEnd: 20,
    backgroundColor: '#DDDDDD',
  },
  viewSearch: {
    backgroundColor: 'rgba(45,45,255,0.2)',
    padding: 10,
    borderRadius: 10,
    margin: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputSearch: {fontSize: 20, padding: 0, flex: 1},
  viewLoading: {position: 'absolute', end: 0, marginEnd: 10},
});

export default LocationsPageStyle;
