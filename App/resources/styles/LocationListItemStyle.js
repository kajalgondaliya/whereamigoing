import {StyleSheet} from 'react-native';
import colors from '../colors';
const LocationListItemStyle = StyleSheet.create({
  viewItem: {
    margin: 10,
    backgroundColor: 'rgba(255,200,0,0.1)',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: colors.black,
  },
  textItem: {padding: 10, fontSize: 15, color: colors.black},
});

export default LocationListItemStyle;
