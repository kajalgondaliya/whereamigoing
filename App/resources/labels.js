const labels = {
  COMPASS_PAGE: 'Compass Page',
  NO_LOCATION_HISTORY: 'Location History Not Available',
  LOCATION_HISTORY: 'Location History',
  LATITUDE: 'Latitude',
  LONGITUDE: 'Longitude',
  DIRECTION: 'Direction',
  DATE: 'Date',
  SEARCH: 'Search..',
};

export default labels;
