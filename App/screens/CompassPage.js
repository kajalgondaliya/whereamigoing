import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Platform,
  Alert,
  PermissionsAndroid,
} from 'react-native';
import {
  doSetCurrentLocation,
  doAddLocation,
  doSetLocationHistory,
} from '../redux/actions/AppAction';
import {angleFromCoordinate, vehicleBearing} from '../resources/util';
import Geolocation from '@react-native-community/geolocation';
import Compass from '../components/CompassView';
import CompassPageStyle from '../resources/styles/CompassPageStyle';
import labels from '../resources/labels';
import moment from 'moment';

class CompassPage extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentLatLong: props.currentLatLong,
      latLongHistory: props.latLongHistory,
      degree: undefined,
      direction: '',
    };
  }
  checkPermissionLocation() {
    // Geolocation.requestAuthorization();
  }
  /**
   *This function getCurrentLocation of the device
   *
   * @memberof CompassPage
   */
  getCurrentLocation() {
    let config = {
      enableHighAccuracy: false,
      timeout: 10000,
      distanceFilter: 1,
    };

    Geolocation.getCurrentPosition(
      this.handleSuccess,
      this.handleError,
      config,
    );
  }
  getWatchLocation() {
    let watchConfig = {
      enableHighAccuracy: true,
      timeout: 10000,
      distanceFilter: 1,
      maximumAge: 0,
    };
    this.watchID = Geolocation.watchPosition(
      this.handleSuccess,
      this.handleError,
      watchConfig,
    );
  }
  handleSuccess = position => {
    if (position !== undefined) {
      this.setState(
        {
          isLocation: true,
          currentLatLong: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          },
        },
        () => {
          let curLat = position.coords.latitude;
          let curLong = position.coords.longitude;
          let lastLat =
            this.props.latLongHistory.length > 0
              ? this.props.latLongHistory[this.props.latLongHistory.length - 1]
                  .latitude
              : 0;
          let lastLong =
            this.props.latLongHistory.length > 0
              ? this.props.latLongHistory[this.props.latLongHistory.length - 1]
                  .longitude
              : 0;

          this.setState(
            {
              degree:
                this.props.latLongHistory.length > 0
                  ? angleFromCoordinate(curLat, curLong, lastLat, lastLong)
                  : 0,
            },
            () => {
              this.props.doSetCurrentLocation(this.state.currentLatLong);
              this.compassRef.spin();
              console.log(this.state.degree);
            },
          );

          const params = Object.assign({}, this.state.currentLatLong);
          const curLocation = {
            latitude: curLat,
            longitude: curLong,
          };
          const endLocation = {
            latitude: lastLat,
            longitude: lastLong,
          };

          params.direction = vehicleBearing(endLocation, curLocation);
          params.dateLabel = moment(new Date()).format('DD MM YYYY, h:mm:ss'); // January 5th 2020, 1:36:04 pm
          params.date = new Date(); // January 5th 2020, 1:36:04 pm

          this.setState({direction: vehicleBearing(endLocation, curLocation)});
          if (this.props.latLongHistory.length > 50) {
            this.props.latLongHistory.shift();
          }
          this.props.doAddLocation(params);
        },
      );
    }
  };
  handleError = error => {
    if (error !== undefined) {
      Alert.alert(error.message);
    }
  };
  doRedirect(screen) {
    this.props.navigation.navigate(screen);
  }
  componentDidMount() {
    this.checkPermissionLocation();
    if (Platform.OS === 'android') {
      PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      ).then(granted => {
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          this.setState({isLocation: true});
          this.getCurrentLocation();
          this.getWatchLocation();
        }
      });
    } else {
      this.getCurrentLocation();
      this.getWatchLocation();
    }
  }
  componentWillUnmount() {
    this.watchID != null && Geolocation.clearWatch(this.watchID);
  }
  render() {
    return (
      <View style={CompassPageStyle.container}>
        <View style={CompassPageStyle.row}>
          <View style={CompassPageStyle.viewFlex}>
            <Text style={CompassPageStyle.textHeader}>
              {labels.COMPASS_PAGE}
            </Text>
          </View>
          <TouchableOpacity onPress={() => this.doRedirect('LocationHistory')}>
            <Text style={CompassPageStyle.textLocationHistory}>
              {labels.LOCATION_HISTORY}
            </Text>
          </TouchableOpacity>
        </View>

        <View style={CompassPageStyle.row}>
          <Text style={CompassPageStyle.textLatitude}>
            {labels.LATITUDE} : {this.state.currentLatLong.latitude.toFixed(4)}
          </Text>
          <Text style={CompassPageStyle.textLongitude}>
            {labels.LONGITUDE} :{' '}
            {this.state.currentLatLong.longitude.toFixed(4)}
          </Text>
          <Text style={CompassPageStyle.textDirection}>
            {labels.DIRECTION} : {this.state.direction}
          </Text>
        </View>

        <Compass
          ref={ref => (this.compassRef = ref)}
          heading={this.state.degree}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentLatLong: state.app.currentLatLong,
    latLongHistory: state.app.latLongHistory,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(
      {doSetLocationHistory, doAddLocation, doSetCurrentLocation},
      dispatch,
    ),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(CompassPage);
