import {combineReducers} from 'redux';
import AppReducer from './AppReducers';

const index = combineReducers({
  app: AppReducer,
});

export default index;
