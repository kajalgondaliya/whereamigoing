import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  Animated,
  Easing,
  Dimensions,
} from 'react-native';
import icons from '../resources/icons';

export default class Compass extends Component {
  constructor(props) {
    super(props);
    this.spinValue = new Animated.Value(0);
    this.state = {
      location: null,
      errorMessage: null,
      heading: props.heading !== undefined ? props.heading : 0,
      truenoth: null,
    };
  }

  componentWillUpdate() {
    this.spin();
  }
  componentDidUpdate(prevProps) {
    console.log(prevProps);

    if (this.props.heading !== prevProps.heading) {
      this.setState({heading: this.props.heading}, () => {
        this.spin();
        console.log(this.spinValue);
      });
    }
  }
  setHeading(heading) {
    this.setState({heading: heading});
  }
  spin() {
    let start = JSON.stringify(this.spinValue);
    let heading = Math.round(this.state.heading);

    let rot = +start;
    let rotM = rot % 360;

    if (rotM < 180 && heading > rotM + 180) rot -= 360;
    if (rotM >= 180 && heading <= rotM - 180) rot += 360;

    rot += heading - rotM;

    Animated.timing(this.spinValue, {
      toValue: rot,
      duration: 300,
      easing: Easing.easeInOut,
    }).start();
  }

  render() {
    let LoadingText = 'Loading...';
    let display = LoadingText;

    if (this.state.errorMessage) display = this.state.errorMessage;

    const spin = this.spinValue.interpolate({
      inputRange: [0, 360],
      outputRange: ['-0deg', '-360deg'],
    });

    display = Math.round(JSON.stringify(this.spinValue));

    if (display < 0) display += 360;
    if (display > 360) display -= 360;
    console.log(display);

    return (
      <View style={styles.container}>
        {/* <Text style={styles.text}>{display + '°'}</Text> */}
        <View style={styles.imageContainer}>
          <Animated.Image
            resizeMode="contain"
            source={icons.COMPASS}
            style={{
              width: deviceWidth - 10,
              height: deviceHeight / 2 - 10,
              left: deviceWidth / 2 - (deviceWidth - 10) / 2,
              top: deviceHeight / 3 - (deviceHeight / 2 - 10) / 2,
              transform: [{rotate: spin}],
            }}
          />
        </View>
        <View style={styles.arrowContainer}>
          <Image
            resizeMode="contain"
            source={icons.ARROW}
            style={styles.arrow}
          />
        </View>
      </View>
    );
  }
}

// Device dimensions so we can properly center the images set to 'position: absolute'
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: '#263544',
    fontSize: 80,
    transform: [
      {translateY: -(deviceHeight / 2 - (deviceHeight / 2 - 10) / 2) - 50},
    ],
  },
  imageContainer: {
    ...StyleSheet.absoluteFillObject,
  },
  arrowContainer: {
    ...StyleSheet.absoluteFillObject,
  },
  arrow: {
    width: deviceWidth / 7,
    height: deviceWidth / 7,
    left: deviceWidth / 2 - deviceWidth / 7 / 2,
    top: deviceHeight / 3 - deviceWidth / 7 / 2,
    opacity: 0.9,
  },
});
