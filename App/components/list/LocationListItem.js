import React, {PureComponent} from 'react';
import {View, Text} from 'react-native';
import LocationListItemStyle from '../../resources/styles/LocationListItemStyle';
import labels from '../../resources/labels';
class LocationListItem extends PureComponent {
  render() {
    const {latitude, longitude, direction,date} = this.props;
    return (
      <View style={LocationListItemStyle.viewItem}>
        <Text style={LocationListItemStyle.textItem}>
          {labels.LATITUDE} : {latitude}
        </Text>
        <Text style={LocationListItemStyle.textItem}>
          {labels.LONGITUDE} : {longitude}
        </Text>
        <Text style={LocationListItemStyle.textItem}>
          {labels.DIRECTION} : {direction}
        </Text>
        <Text style={LocationListItemStyle.textItem}>
          {labels.DATE} : {date}
        </Text>
      </View>
    );
  }
}
export default LocationListItem;
