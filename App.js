import React, {PureComponent} from 'react';
import {View, SafeAreaView} from 'react-native';
import colors from './App/resources/colors';
import AppIndex from './App/screens';

class App extends PureComponent {
  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: colors.colorBackground}}>
        <View style={{flex: 1, backgroundColor: colors.colorBackground}}>
          <AppIndex />
        </View>
      </SafeAreaView>
    );
  }
}
export default App;
